import mysql from 'mysql2';
import os from 'os';
import http from 'http';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'dasha2',
  password: '123',
  database: 'test_db'
});

(async () => {
  // Start connection
  try {
    await connection.promise().connect();
  } catch (err) {
    console.log('Error connecting');
    process.exit();
  }

  // Set up cleanup
  process.on('exit', async () => {
    try {
      await connection.promise().query('DROP TABLE Logs');
      console.log('Table dropped');
    } catch (err) {
      console.log('Error dropping table');
    }

    connection.destroy();
    console.log('Connection destroyed');
  });
  process.on('SIGINT', () => process.exit());

  // Ensure table exists
  try {
    await connection
      .promise()
      .query(
        `CREATE TABLE if not exists Logs (freemem VARCHAR(255), totalmem VARCHAR(255), hostname VARCHAR(255))`
      );
  } catch (err) {
    console.log('Error creating table', err);
    process.exit();
  }
  console.log('Table is present');

  // Insert into created table
  try {
    await connection
      .promise()
      .query(
        `INSERT INTO Logs (freemem, totalmem, hostname)` +
          ` VALUES ('${os.freemem()}', '${os.totalmem()}', '${os.hostname()}')`
      );
  } catch (err) {
    console.log('Error inserting data', err);
    process.exit();
  }

  // Set up http server and request handler
  http
    .createServer(
      async (req: http.IncomingMessage, res: http.ServerResponse) => {
        console.log('Incoming request');

        try {
          const [rows] = await connection.promise().query(`SELECT * FROM Logs`);
          res.writeHead(200, { 'Content-Type': 'application/json' });
          res.end(JSON.stringify(rows));
        } catch (err) {
          res.statusCode = 500;
          res.end();
        }
      }
    )
    .listen(3333, () => {
      console.log('Listening');
    });
})();
